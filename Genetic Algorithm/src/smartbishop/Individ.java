package smartbishop;

import java.util.Random;

/**
 * Created by Alex on 07.12.2018.
 */
public class Individ {
    public double x,y;
    private static Random rand;
    public String Xhrom, Yhrom;
    public final double Pm = 0.2;
    Individ(){
        Xhrom = new String();
        Yhrom = new String();

    }
    // мутация
    public void mutation()
    {
        rand = new Random();
        //DoubleStream randomLongStream = rand.doubles(2,0,1);
        double p1 = rand.nextDouble(), p2 = rand.nextDouble();
        if(p1<Pm) {
            char tmp[] =  Xhrom.toCharArray();
            int k = Math.abs(rand.nextInt())%20 ;
            if(tmp[k] == '1')
                tmp[k] = '0';
            else
                tmp[k] = '1';
            Xhrom = String.valueOf(tmp);
        }
        if(p2<Pm) {
            char tmp[] =  Yhrom.toCharArray();
            int k = Math.abs(rand.nextInt())%20 ;
            if(tmp[k] == '1')
                tmp[k] = '0';
            else
                tmp[k] = '1';
            Yhrom = String.valueOf(tmp);
        }

    }
    public double F() {
        return 0.1 * x + 0.1 * y - 4 * Math.cos(0.8 * x) + 4 * Math.cos(0.8 * y) + 8;
    }

}
