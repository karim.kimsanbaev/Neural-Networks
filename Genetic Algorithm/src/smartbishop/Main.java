package smartbishop;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.jzy3d.chart.AWTChart;
import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.javafx.JavaFXChartFactory;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.maths.Range;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

import java.util.ArrayList;
import java.util.Random;


public class Main extends Application {

    private static ArrayList <Individ> listInd;
    private static String iter1 , iter2, iter3;
    private static Label resultat, l1, l2;
    private static TextArea textField;
    public final double Pc = 0.9;
    public int max;
    public int flag;
    public AWTChart chart;
    private static ArrayList<Point> points1, points2, points3;
    private final int maxGr = 6;
    private final int minGr = -6;  // диапозон от [-6 ; 6]
    public final int N = 50; // количество особей
    private static Random rand;

    public static void main(String[] args) {
        Application.launch(args);
    }


    @Override
    public void start(Stage stage) {

        stage.setTitle("Генетический алгоритм вариант 1");
        // Jzy3d
        JavaFXChartFactory factory = new JavaFXChartFactory();
        chart  = getDemoChart(factory, "offscreen");
        ImageView imageView = factory.bindImageView(chart);

        int initialValue=30;
        iter1 = new String();
        iter2 = new String();
        iter3 = new String();
        textField = new TextArea();
        Button button = new Button("Запустить алгоритм");
        listInd = new ArrayList<>();
        points1 = new ArrayList<>();
        points2 = new ArrayList<>();
        points3 = new ArrayList<>();

        flag =1;
        rand = new Random();
        l2 = new Label("Популяция");
        Spinner<Integer> spinner = new Spinner<Integer>();
        SpinnerValueFactory<Integer> valueFactory = //
                new SpinnerValueFactory.IntegerSpinnerValueFactory(10, 100, initialValue);
        spinner.setValueFactory(valueFactory);
        resultat = new Label();

        Algoritm(initialValue);
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Начальная",
                        "Вторая",
                        "Последняя"

                );
        System.out.println(points2.size());
        ComboBox comboBox = new ComboBox(options);
        comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {


             for(int i=0; i<N && points1.size()!=0; i++){

                chart.removeDrawable(points1.get(i));
                chart.removeDrawable(points2.get(i));
                chart.removeDrawable(points3.get(i));
            }

                if(newValue == "Начальная") {
                    textField.setText(iter1);
                    flag=1;
                    for(int i=0; i<N; i++){
                        chart.add(points1.get(i));
                    }
                }
                if(newValue == "Вторая") {
                    flag=2;
                    textField.setText(iter2);
                    for(int i=0; i<N; i++){
                        chart.add(points2.get(i));
                    }
                }
                if(newValue == "Последняя") {
                    textField.setText(iter3);
                    flag=3;
                    for(int i=0; i<N; i++){
                        chart.add(points3.get(i));
                    }

                }

        });



        // JavaFX
        Pane pane = new Pane();
        Scene scene = new Scene(pane, 1200, 750);
        stage.setScene(scene);
        stage.show();



         factory.addSceneSizeChangedListener(chart, scene);
         //stage.setWidth(500);
        //stage.setHeight(500);

        max = 0;
        for (int i = 1; i < listInd.size(); i++) {
            if (listInd.get(max).F() < listInd.get(i).F())
                max = i;
        }
        resultat.setText("Max:\nx=" + listInd.get(max).x + "\ny=" + listInd.get(max).y);
        //chart.add(new Point(new Coord3d(listInd.get(max).x, listInd.get(max).y,listInd.get(max).F() ), new Color (0,0,0),3));
        button.setOnAction(event -> {

            for(int i=0; i<N && points1.size()!=0; i++){

                chart.removeDrawable(points1.get(i));
                chart.removeDrawable(points2.get(i));
                chart.removeDrawable(points3.get(i));
            }
            Algoritm(spinner.getValue());
            max = 0;
            for (int i = 1; i < listInd.size(); i++) {
                if (listInd.get(max).F() < listInd.get(i).F())
                    max = i;
            }
            resultat.setText("Max:\nx=" + listInd.get(max).x + "\ny=" + listInd.get(max).y);
           // chart.add(new Point(new Coord3d(listInd.get(max).x, listInd.get(max).y,listInd.get(max).F() ), new Color (0,0,0)));



            if(comboBox.getValue() == "Начальная") {
                textField.setText(iter1);
                flag=1;
                for(int i=0; i<N; i++){
                    chart.add(points1.get(i));
                }
            }
            if(comboBox.getValue() == "Вторая") {
                flag=2;
                textField.setText(iter2);
                for(int i=0; i<N; i++){
                    chart.add(points2.get(i));
                }

            }
            if(comboBox.getValue() == "Последняя") {
                flag=3;
                textField.setText(iter3);
                for(int i=0; i<N; i++){
                    chart.add(points3.get(i));
                }
            }

           /* if(comboBox.getValue() == "Начальная")
                textField.setText(iter1);
            if(comboBox.getValue() == "Вторая")
                textField.setText(iter2);
            if(comboBox.getValue() == "Последняя")
                textField.setText(iter3);*/
                });

        l1 = new Label("Количество итераций: ");
        imageView.relocate(10,10);
        resultat.relocate(70, 520);
        button.relocate(130,650);
        spinner.setMaxWidth(80);
        spinner.relocate(250, 600);
        l1.relocate(70,605);
        textField.relocate(520, 10);
        textField.setMinWidth(650);
        textField.setMinHeight(710);
        comboBox.relocate(370,600);
        comboBox.setValue(options.get(0));
        l2.relocate(370, 570);


        //System.out.println(imageView.getX());
        pane.getChildren().addAll(imageView , button,spinner, resultat, l1, textField, comboBox, l2);
    }

    private AWTChart getDemoChart(JavaFXChartFactory factory, String toolkit) {
        // -------------------------------
        // Define a function to plot
        Mapper mapper = new Mapper() {
            @Override
            public double f(double x, double y) {
                return 0.1*x +0.1*y-4*Math.cos(0.8*x)+4*Math.cos(0.8*y)+8;
                // return Math.abs(0.2*x)-Math.abs(0.2*y)+4;
            }
        };

        // Define range and precision for the function to plot
        Range range = new Range(-6, 6);
        int steps = 80;

        // Create the object to represent the function over the given range.
        final Shape surface = Builder.buildOrthonormal(mapper, range, steps);
        surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(), surface.getBounds().getZmax(), new Color(1, 1, 1, .5f)));
        surface.setFaceDisplayed(true);
        surface.setWireframeDisplayed(false);

        // -------------------------------
        // Create a chart
        Quality quality = Quality.Advanced;
        //quality.setSmoothPolygon(true);
        //quality.setAnimated(true);

        // let factory bind mouse and keyboard controllers to JavaFX node
        AWTChart chart = (AWTChart) factory.newChart(quality, toolkit);
        chart.getScene().getGraph().add(surface);
        return chart;
    }

    private String textIter(){
        String str = new String();
        str = "x                  y                 Хромосомы X\t                Хромосомы Y\t           Цел. фун.   Приспос.\n";
        for(int j=0; j<N; j++){
            str+= String.format("%.4g",listInd.get(j).x)+"\t" +String.format("%.4g",listInd.get(j).y)+"\t" +listInd.get(j).Xhrom+"\t  "
                    +listInd.get(j).Yhrom+"\t" + String.format("%.4g",listInd.get(j).F())+"\t" + String.format("%.4g",listInd.get(j).F())+"\n";
        }
        return str;
    }

    private void Algoritm (int iteration) {

        //генерация 50 особей
        listInd.clear();


        points1.clear();
        points2.clear();
        points3.clear();
        for (int t = 0; t < N; t++) {

            // генерирую хромосомы
            double first = rand.nextDouble() * 12 - 6;//randomX.toArray()[t];
            double second = rand.nextDouble() * 12 - 6;//randomY.toArray()[t];
            Individ tmp = new Individ();
            tmp.x = first;
            tmp.y = second;
            tmp.Xhrom = Integer.toBinaryString((int) ((first + maxGr) * (Math.pow(2, 20) - 1) / maxGr / 2));
            tmp.Yhrom = Integer.toBinaryString((int) ((second + maxGr) * (Math.pow(2, 20) - 1) / maxGr / 2));
            // Дополняю хробосомы до 20 бит
            if (tmp.Xhrom.length() < 20) {
                String zero = new String();
                for (int i = 0; i < 20 - tmp.Xhrom.length(); i++)
                    zero += '0';
                tmp.Xhrom = zero + tmp.Xhrom;

            }
            if (tmp.Yhrom.length() < 20) {
                String zero = new String();
                for (int i = 0; i < 20 - tmp.Yhrom.length(); i++)
                    zero += '0';
                tmp.Yhrom = zero + tmp.Yhrom;

            }
            listInd.add(tmp);
            points1.add(new Point(new Coord3d(tmp.x, tmp.y,tmp.F() ), new Color (0,0,0), 4));
        }
        iter1 = textIter();


        for (int i = 1; i < iteration; i++) {
            // генерирую потомков (внутри мутация)
            genChildren();
            // турнир
            turnir();
            if (i == 2) {
                iter2 = textIter();
                for (int k=0; k<N; k++)
                    points2.add(new Point(new Coord3d(listInd.get(k).x, listInd.get(k).y,listInd.get(k).F() ), new Color (0,0,0), 4));
            }
            if (i == iteration - 1) {
                iter3 = textIter();
                for (int k=0; k<N; k++)
                    points3.add(new Point(new Coord3d(listInd.get(k).x, listInd.get(k).y,listInd.get(k).F() ), new Color (0,0,0), 4));
            }
        }


        }


    private void genChildren(){
        //cортировка по функции приспособленности
        for(int i=0; i<listInd.size(); i++)
        {
            for(int j = i; j<listInd.size(); j++){
                if(listInd.get(i).F() >  listInd.get(j).F()){
                    Individ tmp = listInd.get(i);
                    listInd.set(i,listInd.get(j));
                    listInd.set(j,tmp);
                }

            }
        }

        // скрещиваю лучших с лучшими
        for(int i=0; i<N; i+=2){
            //System.out.println(listInd.size());
            double p = rand.nextDouble();
            if(p< Pc) {

                int k = rand.nextInt(19);

                Individ child1 = new Individ();
                Individ child2 = new Individ();
                child1.Xhrom = listInd.get(i).Xhrom.substring(0,k) + listInd.get(i+1).Xhrom.substring(k);
                child1.Yhrom = listInd.get(i).Yhrom.substring(0,k) + listInd.get(i+1).Yhrom.substring(k);
                child2.Xhrom = listInd.get(i).Xhrom.substring(0,k) + listInd.get(i+1).Xhrom.substring(k);
                child2.Yhrom = listInd.get(i).Yhrom.substring(0,k) + listInd.get(i+1).Yhrom.substring(k);

                //мутация потомков
                child1.mutation();
                child2.mutation();

                child1.x = ((double)Integer.parseInt(child1.Xhrom,2)/(Math.pow(2, 20)-1)*2*maxGr)-maxGr;
                child2.x = ((double)Integer.parseInt(child2.Xhrom,2)/(Math.pow(2, 20)-1)*2*maxGr)-maxGr;
                child1.y = ((double)Integer.parseInt(child1.Yhrom,2)/(Math.pow(2, 20)-1)*2*maxGr)-maxGr;
                child2.y = ((double)Integer.parseInt(child2.Yhrom,2)/(Math.pow(2, 20)-1)*2*maxGr)-maxGr;

                //добавляем детей в список
                listInd.add(child1);
                listInd.add(child2);
            }
        }

    }

    private  void turnir(){
        ArrayList <Individ> newInd  = new ArrayList<>(); // новый список с индивидумами
        while(newInd.size()!= 50){
            int count = listInd.size();
            int a = rand.nextInt(count), b, c;
            do {
                b=rand.nextInt(count);
            } while(a == b);
            c = (listInd.get(a).F() > listInd.get(b).F()) ? a : b;
            newInd.add(listInd.get(c));
            listInd.remove(c);
        }
        listInd.clear();
        listInd = newInd;
    }
}
