import random
import sys
import numpy as np
import bitstring
import pandas as pd

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import QApplication, QMainWindow, QSizePolicy, QPushButton, QLabel, QLineEdit
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

from matplotlib.figure import Figure

LEFT = -1.0
RIGHT = 1.0


class Window(QMainWindow):

    def __init__(self, ga):
        super().__init__()
        screen_geometry = QApplication.desktop().availableGeometry()
        self.width = 900
        self.height = 600
        self.left = int((screen_geometry.width() - self.width) / 2)
        self.top = int((screen_geometry.height() - self.height) / 2)
        self.title = 'Генетический алгоритм'
        self.ga = ga
        self.initUI()
        self.n_step = 30
        self.cur_step = 0

    def initUI(self):
        self.setStyleSheet("QMainWindow { background-color: white }")
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # установка холста
        canvas = PlotCanvas(self, width=7, height=6)
        canvas.move(0, 0)
        canvas.plot(self.ga)
        self.canvas = canvas

        button_step = QPushButton('Сделать шаг', self)
        button_step.clicked.connect(self.on_click_button_step)
        button_step.move(700, 200)
        button_step.setFixedSize(QSize(120, 30))

        button_all_steps = QPushButton('Сделать все шаги', self)
        button_all_steps.clicked.connect(self.on_click_button_all_steps)
        button_all_steps.move(700, 250)
        button_all_steps.setFixedSize(QSize(120, 30))

        button_restart = QPushButton('Сброс', self)
        button_restart.clicked.connect(self.on_click_button_restart)
        button_restart.move(700, 500)
        button_restart.setFixedSize(QSize(120, 30))

        label1 = QLabel("Кол-во итераций", self)
        label1.move(700, 50)

        label3 = QLabel("Текущая итерация", self)
        label3.move(700, 80)

        label2 = QLabel("Лучшая особь", self)
        label2.move(700, 120)

        best_individual_edit = QLineEdit(self)
        best_individual_edit.move(700, 150)
        best_individual_edit.setFixedSize(QSize(120, 20))
        best_individual_edit.setAlignment(Qt.AlignHCenter)
        best_individual_edit.setReadOnly(True)
        self.best_individual_edit = best_individual_edit

        cur_step_edit = QLineEdit(self)
        cur_step_edit.move(800, 85)
        cur_step_edit.setFixedSize(QSize(50, 20))
        cur_step_edit.setAlignment(Qt.AlignHCenter)
        cur_step_edit.setText("0")
        cur_step_edit.setReadOnly(True)
        self.cur_step_edit = cur_step_edit

        n_steps_edit = QLineEdit(self)
        n_steps_edit.move(800, 55)
        n_steps_edit.setFixedSize(QSize(50, 20))
        n_steps_edit.setAlignment(Qt.AlignHCenter)
        n_steps_edit.setReadOnly(False)
        n_steps_edit.setMaxLength(4)
        n_steps_edit.setText("30")
        int_validator = QIntValidator(self)
        int_validator.setRange(1, 1000)
        n_steps_edit.setValidator(int_validator)
        n_steps_edit.textChanged.connect(self.set_n_steps_event)


        self.n_steps_edit = n_steps_edit

        self.show()

    def on_click_button_all_steps(self):
        if self.cur_step < self.n_step:
            for i in np.arange(self.n_step - self.cur_step):
                if i == 0 or i == 1 or i == self.n_step - 1:
                    self.save_population(i)
                self.ga.step()
                self.cur_step += 1
            self.canvas.axes.clear()
            self.update_best_individual()
            self.canvas.plot(self.ga)
            self.cur_step_edit.setText(str(self.cur_step))


    def on_click_button_step(self):
        if self.cur_step < self.n_step:
            self.canvas.axes.clear()
            self.ga.step()
            self.update_best_individual()
            self.canvas.plot(self.ga)
            self.cur_step += 1
            self.cur_step_edit.setText(str(self.cur_step))

    def on_click_button_restart(self):
        self.cur_step = 0
        self.cur_step_edit.setText(str(self.cur_step))
        self.ga.restart()
        self.best_individual_edit.setText("")
        self.canvas.axes.clear()
        self.canvas.plot(self.ga)

    def update_best_individual(self):
        x, y = self.ga.getBestIndividual()
        self.best_individual_edit.setText("x = " + str(round(x, 2)) + ", y = " + str(round(y, 2)))

    def set_n_steps_event(self):
        if self.cur_step == 0:
            self.n_step = int(self.n_steps_edit.text())

    def save_population(self, i):
        population = self.ga.getPopulation()
        dict = {'Nomer osobi v population': [i for i in np.arange(1, self.ga.n + 1)],
                'x': [round(ind.x, 2) for ind in population],
                'Chomosoma': [ind.chromosome for ind in population],
                'y(x)': [round(ind.y, 2) for ind in population],
                'f(x)': [round(ind.fitness, 2) for ind in population]}
        df = pd.DataFrame(data=dict)
        df['mean'] = round(df['f(x)'].mean(),2)
        df.to_csv("population_"+str(i)+".csv")

class PlotCanvas(FigureCanvas):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111)

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def plot(self, ga):
        ax = self.axes
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_title("y = 3.5 * x + sin(47 * pow(x, 2) + 2) - 6 * pow(x, 2)")
        x = np.arange(LEFT, RIGHT, 0.01)
        y = [func(x) for x in x]

        ax.grid()
        ax.plot(x, y, c='black')

        x_ga, y_ga = ga.getPopulationXY()
        ax.scatter(x_ga, y_ga, c="red")
        ax.figure.canvas.draw()
        self.show()


class Individual:
    """
    Особь для генетического алгоритма
    x - фенотип
    y - значение функции func(x)
    fitness - приспособленность
    chromosome - двоичное кодирование фенотипа 20 бит
    """

    def __init__(self, x):
        self.chromosome = ""
        self.x = 0.0
        self.y = 0.0
        self.fitness = 0.0
        if isinstance(x, float):
            self.chromosome = bitstring.BitArray(float=x, length=32)[0:20].bin
        if isinstance(x, str):
            self.chromosome = x
        self.calculate()

    def mutation(self):
        """
        Мутация хромосомы - случайное инвертирование бита
        :return: None
        """
        point = random.randint(0, 19)
        if self.chromosome[point] == '1':
            self.chromosome = self.chromosome[:point] + "0" + self.chromosome[point + 1:]
        else:
            self.chromosome = self.chromosome[:point] + "1" + self.chromosome[point + 1:]
        self.calculate()

    def calculate(self):
        """
        Вычисление фенотипа и приспособленности при
        изменение хромосомы
        :return: None
        """
        self.x = bitstring.BitArray(bin=self.chromosome + '0' * 12).float
        # если в процессе изменения хромосомы получилось не число
        # меняем фенотип на RIGHT
        if np.math.isnan(self.x):
            self.x = RIGHT
            self.chromosome = bitstring.BitArray(float=self.x, length=32)[0:20].bin
        self.y = func(self.x)
        self.fitness = np.exp(self.y)


    def check(self, left, right):
        """
        Проверка принадлежности фенотипа
        интервалу [left, right]
        В случае выхода фенотипа за пределы
        интервала, значение фенотипа устанавливается
        ближайшей границей интервала +-eps
        :param left: левая граница
        :param right: правая граница
        :return: None
        """
        flag = False
        if self.x > right:
            self.x = right
            flag = True
        if self.x < left:
            self.x = left
            flag = True
        if flag:
            self.chromosome = bitstring.BitArray(float=self.x, length=32)[0:20].bin
            self.calculate()


def func(x):
    return 3.5 * x + np.sin(47 * pow(x, 2) + 2) - 6 * pow(x, 2)


def crossing(ind1: Individual, ind2: Individual) -> tuple:
    """
    Реализация одноточечного кроссинговера
    для родителей ind1 и ind2
    :param ind1: первая особь Individual
    :param ind2: вторая особь Individual
    :return: потомки ind1 и ind2
    """
    point = random.randint(0, 19)
    chromosome1 = ind1.chromosome
    chromosome2 = ind2.chromosome
    result1 = chromosome1[0:point] + chromosome2[point:]
    result2 = chromosome2[0:point] + chromosome1[point:]
    return Individual(result1), Individual(result2)


class GA:
    def __init__(self, ps=0.85, pm=0.1, n=50):
        """
        :param ps: вероятность скрещивания
        :param pm: вероятность мутации
        :param n: количество особей в популяции
        """
        self.Ps = ps
        self.Pm = pm
        self.n = n
        self.population = self.initialPopulation(n)

    def restart(self):
        self.population = self.initialPopulation(self.n)

    @staticmethod
    def initialPopulation(n):
        """
        :param n: размер популяции
        :return: массив особей размера n
                равномерно распределённых
                на отрезке (LEFT, RIGHT)
        """
        array = [(random.random() - RIGHT / (RIGHT - LEFT)) * (RIGHT - LEFT) for i in range(n)]
        return [Individual(x) for x in array]

    def getBestIndividual(self):
        best = self.population[0]
        for i in np.arange(len(self.population)):
            if self.population[i].y > best.y:
                best = self.population[i]
        return best.x, best.y

    def getPopulationXY(self):
        x = [ind.x for ind in self.population]
        y = [ind.y for ind in self.population]
        return x, y

    def getPopulation(self):
        return self.population

    def step(self):
        """
        Один шаг генетического алгоритма
        :return: None
        """
        children = []
        # формирование потомков
        for i in np.arange(self.n):
            rand = random.random()
            if rand <= self.Ps:
                parent1 = self.population[random.randint(0, self.n - 1)]
                parent2 = self.population[random.randint(0, self.n - 1)]
                child1, child2 = crossing(parent1, parent2)
                # проверка координат потомков для особей, чьи координаты
                # находятся за пределами интервала [LEFT, RIGHT]
                # установка координат в ближайший конец интервала
                child1.check(LEFT, RIGHT)
                child2.check(LEFT, RIGHT)
                children.append(child1)
                children.append(child2)
        # мутация потомков
        for i in np.arange(len(children)):
            rand = random.random()
            if rand <= self.Pm:
                children[i].mutation()
                children[i].check(LEFT, RIGHT)
        # объединение родителей и детей в одну популяцию
        self.population = self.population + children
        # редукция популяции
        self.reduction()

    def reduction(self):
        """
        Производится редукция текущей популяции
        используя алгоритм "рулетки"
        :return: None
        """
        new_population = []
        while len(new_population) < self.n:
            sum_fitness = 0.0
            for j in np.arange(len(self.population)):
                sum_fitness += self.population[j].fitness
            cur_sum_fitness = 0.0
            relative_fitness = []
            # находим относительные приспособленности
            # для целевой функции min
            for j in np.arange(len(self.population)):
                cur_sum_fitness += self.population[j].fitness
                relative_fitness.append(cur_sum_fitness / sum_fitness)
            relative_fitness[len(relative_fitness) - 1] = 1.0
            rand = random.random()
            for j in np.arange(len(relative_fitness)):
                # увеличиваем новую популяцию
                if relative_fitness[j] > rand:
                    new_population.append(self.population[j])
                    self.population.remove(self.population[j])
                    break
        self.population = new_population


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Window(GA())
    sys.exit(app.exec_())
